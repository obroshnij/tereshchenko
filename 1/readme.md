# User Class

The class in question has an obvious duplication problem. What are possible ways
to remove duplication? What are the pros and cons of each solution?

A couple of things to keep in mind:

- the user may want to be able to read/write all attributes that Github API returns
- what if Github updates their API to return more attributes or omit some existing ones?
