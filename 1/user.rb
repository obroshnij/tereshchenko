require 'net/http'
require 'json'

class User
  def initialize(username)
    uri = URI.parse "https://api.github.com/users/#{username}"
    @data = parse_user_data data_request(uri)
    populate_api_methods
  end

  private

  def parse_user_data(data)
    JSON.parse data, symbolize_names: true
  end

  def data_request(uri)
    Net::HTTP.get uri
  end

  def populate_api_methods
    @data.each do |attribute, value|
      self.class.send(:define_method, attribute.to_s) do
        instance_variable_get("@#{attribute}") || value
      end

      self.class.send(:define_method, "#{attribute}=") do |arg|
        instance_variable_set("@#{attribute}", arg.to_s.empty? ? nil : arg.to_s)
      end
    end
  end

  def populate_api_vars
    @data.each do |attribute, value|
      instance_variable_set("@#{attribute}", value)
    end
  end
end
