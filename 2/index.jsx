import React from 'react';
import ReactDOM from 'react-dom';

class Gapi extends React.Component {
  render() {
    return (
      <div>
        <GapiUserComponent/>
      </div>
    );
  }
}

class GapiUserComponent extends React.Component {
  constructor(props) {
    super(props);
    this.performSearch = this.performSearch.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setUserData = this.setUserData.bind(this);
    this.state = {username: '', userdata: {}, err: false};
  }

  performSearch(e) {
    if (e.key === 'Enter') {
      fetch(
        "https://api.github.com/users/" + this.state.username,
        {
          method: 'GET',
          headers: { 'Accept': 'application/json' }
        })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.message != 'Not Found'){
          this.setUserData(responseJson);
          this.setUserError(false);
        } else {
          this.setUserError(true);
          this.setUserData({});
        }
      })
    }
  }

  setUserData(data){
    this.setState({userdata: data});
  }

  setUserError(flag){
    this.setState({err: flag});
  }

  handleInputChange(event){
    this.setState({username: event.target.value});
  }

  render() {
    return(
      <div>
        <div className='content form-group text-center'>
          <input type='text' value={this.state.username} id='username-input' onKeyPress={this.performSearch} onChange={this.handleInputChange}></input>
        </div>
        <GapiUserData userdata={this.state.userdata}/>
        <GapiUserError err={this.state.err}/>
      </div>
    )
  }
}

class GapiUserData extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let hashElements = Object.keys(this.props.userdata);
    if(!Object.keys(hashElements.length)){
      return null;
    } else {
      let content = Object.keys(this.props.userdata).map((hashKey) => {
        return <DataComponent key={hashKey} elementKey={hashKey} elementValue={this.props.userdata[hashKey]}/>
      })
      return (
        <div className='gapi-user-data'>
          {content}
        </div>
      )
    }
  };
}

function DataComponent(props){
  return(
    <div>
       <span><DataComponentKey elementKey={props.elementKey}/><DataComponentValue value={props.elementValue}/></span>
    </div>
  )
}

function GapiUserError(props){
  if(props.err){
    return(
      <div className='gapi-user-data'>
         No such user found
      </div>
    )
  } else {
    return null
  }
  
}

function DataComponentKey(props){
  return(
    <div className='component-key'>
      <b>{props.elementKey}</b>
    </div>
  )
}

class DataComponentValue extends React.Component {
  constructor(props) {
    super(props);
    this.componentLinkHandler = this.componentLinkHandler.bind(this);
  }

  componentLinkHandler(value){
    if (/\http/.test(value)) {
      return <a className='more-data' href={value}>{value}</a>;
    } else {
      return value;
    }
  }

  render(){
    return(
      <div className='component-value'>
        {this.componentLinkHandler(this.props.value)}
      </div>
    )
  }
}

ReactDOM.render(<Gapi/>, document.getElementById('gapi'));